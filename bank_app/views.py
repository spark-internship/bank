import json
from django.http import JsonResponse
from django.shortcuts import render
from .models import Transfer,Customer
from django.db.models import Q


#view for showing home page
def home_view(request):
    return render(request,'home.html')


#view for showing all transactions
def transactions(request):
    trans=Transfer.objects.all()
    return render(request,'transactions.html',{'transactions':trans})

#view for showing all customers
def all_customers(request):
    custo=Customer.objects.all()
    return render(request, 'all_customers.html',{'customers':custo})


#view for showing selected customer
def customer(request,id):
    try:
        customer=Customer.objects.get(id=id)
    except:
        customer=None

    other_customers = Customer.objects.filter(~Q(id = id))    #not equal condition,ie all customers excluding selected customer

    return render(request,'customer.html',{'customer':customer,'cs':other_customers})



#this view is for transfer amount,which is calling from transaction.html by using ajax call
def transfer(request):
    if request.method=='POST':
        from_cust = request.POST['from_customer']
        to_cust=request.POST['to_customer']
        amount = request.POST['amount']

        #deducting amount from customer
        from_customer=Customer.objects.get(id=from_cust)
        f_balance=from_customer.current_balance
        f_new_balance=f_balance- int(amount)
        from_customer.current_balance=f_new_balance
        from_customer.save()

        #adding amount to to_customer
        to_customer = Customer.objects.get(id=to_cust)
        t_balance = to_customer.current_balance
        t_new_balance = t_balance + int(amount)
        to_customer.current_balance = t_new_balance
        to_customer.save()

        #adding transaction record
        Transfer.objects.create(from_customer=from_customer,to_customer=to_customer,amount=amount)

        data = {
            'new_amount': from_customer.current_balance
        }

        return JsonResponse(data, safe=False)

