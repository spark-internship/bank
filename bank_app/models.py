from django.db import models

# Create your models here.

class Customer(models.Model):
    name = models.CharField(max_length=200)
    email=models.EmailField(null=False,unique=True)
    account_number=models.IntegerField(unique=True)
    TYPE_CHOICES = (
        ('Current Account', 'Current Account'),
        ('Savings Account', 'Savings Account'),
        ('Fixed Deposit Account', 'Fixed Deposit Account'),
    )

    account_type=models.CharField(max_length=21, choices=TYPE_CHOICES)
    current_balance = models.IntegerField(null=False)
    city=models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Transfer(models.Model):
    from_customer=models.ForeignKey(Customer,on_delete=models.SET_NULL,null=True,related_name='from_cutomer')
    to_customer=models.ForeignKey(Customer,on_delete=models.SET_NULL,null=True,related_name='to_customer')
    amount=models.IntegerField()
    trans_date=models.DateTimeField(auto_now_add=True)



